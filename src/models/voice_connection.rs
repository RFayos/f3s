pub struct VoiceConnection<'a> {
    id: Option<u32>,
    pub(crate) user_id: &'a str,
    pub(crate) channel_id: &'a str,
    pub(crate) start_date: u32,
    pub(crate) end_date: u32,
}

impl<'a> VoiceConnection<'a> {
    pub fn create(user_id: &'a str, channel_id: &'a str, start_date: u32, end_date: u32) -> Self {
        Self {
            id: None,
            user_id,
            channel_id,
            start_date,
            end_date,
        }
    }
}
