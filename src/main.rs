mod models;
mod repositories;

use std::fmt::Debug;
use std::time::SystemTime;
use serenity::all::{ActionExecution, ChannelId, GuildId, Presence, Ready, VoiceServerUpdateEvent, VoiceState};
use serenity::async_trait;
use serenity::model::channel::Message;
use serenity::prelude::*;
use crate::models::voice_connection::VoiceConnection;
use crate::repositories::voice_connection_repository::VoiceConnectionRepository;

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, msg: Message) {
        if msg.content == "!ping" {
            if let Err(why) = msg.channel_id.say(&ctx.http, "Pong!").await {
                println!("Error sending message: {why:?}");
            }
        }
    }

    // async fn ready(&self, ctx: Context, data_about_bot: Ready) {
    //     dbg!(&data_about_bot);
    // }

    async fn voice_state_update(&self, ctx: Context, old: Option<VoiceState>, new: VoiceState) {
        let user_id = &new.user_id.get();
        let channel_id = &new.channel_id.unwrap_or(Default::default());
        let date = SystemTime::now();

        // Creates VoiceConnection instance
        let voice_connection = VoiceConnection::create(&user_id.to_string(), &channel_id.get().to_string(), 0, 1);

        // Saves voiceConnection in database
        VoiceConnectionRepository::save(&voice_connection);
    }
}

#[tokio::main]
async fn main() {
    // Login with a bot token from the environment
    // let token = env::var("DISCORD_TOKEN").expect("Expected a token in the environment");
    let token = String::from("MTIzMjYwNzA3NTEwOTA0NDI1NA.GWft-5.05D1i0NNfAPjF2flcelrWXMYOajeGsYgYDjBIQ");
    // Set gateway intents, which decides what events the bot will be notified about
    let intents = GatewayIntents::GUILD_MESSAGES
        | GatewayIntents::DIRECT_MESSAGES
        | GatewayIntents::GUILD_VOICE_STATES
        | GatewayIntents::MESSAGE_CONTENT;

    // Create a new instance of the Client, logging in as a bot.
    let mut client =
        Client::builder(&token, intents).event_handler(Handler).await.expect("Err creating client");

    // Start listening for events by starting a single shard
    if let Err(why) = client.start().await {
        println!("Client error: {why:?}");
    }

}

fn print_today_connections(voice_connections: &Vec<VoiceConnection>){

}